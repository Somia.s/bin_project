# Évaluation des graphes et réseaux d’interactions

## Analyse du réseau statique
- Données: networkFGF8.graphml issu de Cytoscape dans le dossier Input
### Enrichissement fonctionnel
- Plugin: BiNGO
- Résultats:
    - GO Processus biologique: output_GO_biologicalProcess.bgo dans le dossier Output
    - GO Composant cellulaire: output_GO_cellularComponent.bgo  dans le dossier Output
### Analyse topologique
- Script: igraph.py dans le dossier Scripts 
- Language: Python 3
- Librairies nécessaires: igraph version 0.8.3 et matplotlib.pyplot
- Résultat: degres.png et output_from_igraph.out dans le dossier Output

## Analyse du modèle dynamique: Équations différentielles
- Données: Goldbeter A, Gonze D, Pourquié O. Sharp developmental thresholds defined through bistability by antagonistic gradients of retinoic acid and FGF signaling. _Dev Dyn_, 2007 Jun;236(6):1495-508.
- Script: equations_differentielles.R dans le dossier Scripts
- Language: R
- Librairies nécessaires: deSolve et phaseR

