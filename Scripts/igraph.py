from igraph import *
import matplotlib.pyplot as plt

## Importer le graphique qui a été crée sur Cytoscape (base de données PSICQUIC)
g = Graph.Read_GraphML("../Output/networkFGF8.graphml")

## Afficher les premières caractéristiques du réseau
summary(g)

## Propriétés structurales du graphiques

# Nombre de degrés de chaque sommet
degrees = g.degree()
print("\nValeurs de tous les degrés de chaque sommet:")
print(degrees)
print("\nLe noeud avec le nombre de degrés le plus grand:")
print(g.vs.select(_degree = max(degrees))["name"])
# Histogramme des degrés
plt.style.use('ggplot')
plt.hist(degrees, bins = 75)
plt.xlabel("Degré")
plt.ylabel("Nombre de sommets")
plt.title("Histogramme du degré des sommets")
plt.show()


## Centralité intermédiaire ou Betweenness des arêtes
ebs = g.edge_betweenness()
print("\nValeurs de la centralité intermédiaire (ou betweenness) de chaque arête:")
print(ebs)
# Valeur de la betweenness maximale
print("\nValeur de la centralité intermédiaire maximale:")
max_eb = max(ebs)
print(max_eb)

## Déterminer quelles sont les connexions qui ont la plus grande centralité entre les deux
print("\nLes tuples de connexions qui ont la plus grande centralité entre elles:")
for idx, eb in enumerate(ebs):
	if eb == max_eb:
		print(g.es[idx]["name"], g.es[idx].tuple)
		
plt.style.use('ggplot')
plt.hist(ebs, bins = 50)
plt.xlabel("Centralité intermédiaire ou Betweenness")
plt.ylabel("Nombre d'arêtes")
plt.title("Histogramme de la centralité intermédiaire (ou betweenness) des arêtes")
plt.show()
